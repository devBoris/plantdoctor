import { fromObject } from '@nativescript/core'

export function HomeViewModel() {
  const viewModel = fromObject({})

  return viewModel
}
