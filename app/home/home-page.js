import { HomeViewModel } from './home-view-model'
import { Http, ApplicationSettings } from '@nativescript/core'
import { fromObjectRecursive } from 'tns-core-modules/data/observable'

import { requestPermissions } from '@nativescript/camera'
import { ImageSource, knownFolders, path } from '@nativescript/core'
const camera = require('@nativescript/camera')
const { Image } = require('@nativescript/core')
const isAvailable = camera.isAvailable()
const source = new ImageSource()

export function onNavigatingTo(args) {
  const component = args.object
  page.actionBarHidden = true
  component.bindingContext = new HomeViewModel()
  page.bindingContext = bindingContext
}

export function Logout(args) {
  ApplicationSettings.setString('token', 'defaultValue')
  const button = args.object
  const page = button.page
  page.frame.navigate({
    moduleName: 'main/main-page',
    clearHistory: true, // Clear navigation history
    animated: true,
    transition: {
      name: 'slide',
    },
  })
}
export function onDisplayButtonTap(args) {
  const diagnoseScrollView = args.object.page.getViewById('diagnoseScrollView')
  diagnoseScrollView.visibility =
    diagnoseScrollView.visibility === 'visible' ? 'collapsed' : 'visible'
}

export function takePicture(args) {
  // cameraModule.takePicture().then(function(picture) {
  //   myImage.imageSource = picture;
  // })
  requestPermissions().then(
    function success() {
      // permission request accepted or already granted
      // ... call camera.takePicture here ...
      const options = {
        width: 300,
        height: 300,
        keepAspectRatio: false,
        saveToGallery: true,
      }

      camera
        .takePicture(options)
        .then(function (imageAsset) {
          console.log(
            'Size: ' +
              imageAsset.options.width +
              'x' +
              imageAsset.options.height
          )
          console.log('keepAspectRatio: ' + imageAsset.options.keepAspectRatio)
          console.log(
            'Photo saved in Photos/Gallery for Android or in Camera Roll for iOS'
          )
          //            source.fromAsset(imageAsset)
          //   .then((imageSource: ImageSource) => {
          //   const folderPath: string = knownFolders.documents().path;
          //   const fileName: string = "test.jpg";
          //   const filePath: string = path.join(folderPath, fileName);
          //   const saved: boolean = imageSource.saveToFile(filePath, "jpg");

          //   if (saved) {
          //       console.log("Gallery: " + this._dataItem.picture_url);
          //       console.log("Saved: " + filePath);
          //       console.log("Image saved successfully!");
          //   }
          // });
        })
        .catch(function (err) {
          console.log('Error -> ' + err.message)
        })
    },
    function failure() {
      // permission request rejected
      // ... tell the user ...
    }
  )
}
