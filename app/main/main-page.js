import { Http } from '@nativescript/core'
import { ApplicationSettings } from '@nativescript/core'

// export const pageLoaded = (args) => {
//   var token = ApplicationSettings.getString('token', 'defaultValue')

//   if (token !== 'defaultValue') {
//     page.frame.navigate('tab-navigation/tab-navigation-page')
//   }
// }

export async function openSignIn(args) {
  var token = ApplicationSettings.getString('token', 'defaultValue')
  const button = args.object
  const page = button.page
  if (token !== 'defaultValue') {
    page.frame.navigate('tab-navigation/tab-navigation-page')
  } else {
    const opts = {
      context: null,
      closeCallback: () => console.log('dialog closed'),
    }
    const modalPagePath = 'auth/signIn/signIn'
    const context = {
      closeCallback: opts.closeCallback,
    }
    page.showModal(
      modalPagePath,
      context,
      async () => {
        var token = ApplicationSettings.getString('token', 'defaultValue')
        if (token !== 'defaultValue') {
          page.frame.navigate('tab-navigation/tab-navigation-page')
        }
        console.log('Dialog closed')
        // page.frame.navigate('tab-navigation/tab-navigation-page')
      },
      false
    )
  }
}
