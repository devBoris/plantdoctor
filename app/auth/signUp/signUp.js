import { fromObjectRecursive } from 'tns-core-modules/data/observable'
import { Http } from '@nativescript/core'

export function closeSignUp(args) {
  const button = args.object
  const page = button.page
  page.closeModal()
}

export function onTap(args) {
  const button = args.object
  const page = button.page
  page.frame.navigate('tab-navigation/tab-navigation-page')
}
export function onOpenSignIn(args) {
  const button = args.object
  const page = button.page
  const opts = {
    context: null,
    closeCallback: () => console.log('dialog closed'),
  }
  const modalPagePath = 'auth/signIn/signIn'
  const context = {
    closeCallback: opts.closeCallback,
  }
  page.showModal(
    modalPagePath,
    context,
    async () => {
      console.log('Dialog closed')
      // page.frame.navigate('tab-navigation/tab-navigation-page')
    },
    false
  )
  const signUp = args.object.page.getViewById('signUp')
  signUp.visibility = signUp.visibility === 'visible' ? 'collapsed' : 'visible'
}

const model = {
  user: { username: '', email: '', password: '', confirmPassword: '' },
}
const bindingContext = fromObjectRecursive(model)

export const pageLoaded = (args) => {
  const page = args.object
  page.bindingContext = bindingContext
}
// Access the values from the bindingContext in your signUp function
export async function signUp(args) {
  const button = args.object
  const page = button.page
  const username = bindingContext.get('user').username
  const email = bindingContext.get('user').email
  const password = bindingContext.get('user').password
  const confirmPassword = bindingContext.get('user').confirmPassword

  const response = await Http.request({
    url: 'https://plant-doctor-db.vercel.app/api/users/signup',
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    content: JSON.stringify({
      email,
      password,
      confirmPassword,
      username,
    }),
  })
  if (response.statusCode === 200 || response.statusCode === 201) {
    const content = JSON.parse(response.content.toString()) // Parse JSON response
    if (content.error) {
      console.log('error:', content.error)
      alert('Error: ' + content.error) // Display error message
    } else {
      console.log('success:', content)
      alert(
        `Hello ${
          response.content.toJSON().username
        } you account has been created successfully, Sign In now...`
      )
    }
  } else {
    console.log('error:', response.statusCode, response.content.toJSON().error)
    alert(
      `Error: ${response.content.toJSON().error}, code : ${response.statusCode}`
    )
  }
}
