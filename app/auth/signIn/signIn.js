import { Http, ApplicationSettings } from '@nativescript/core'
import { fromObjectRecursive } from 'tns-core-modules/data/observable'

export function closeSignIn(args) {
  const button = args.object
  const page = button.page
  var token = ApplicationSettings.getString('token', 'defaultValue')
  console.log(token)
  page.closeModal()
}

const model = {
  user: { email: '', password: '' },
}
const bindingContext = fromObjectRecursive(model)
export const pageLoaded = (args) => {
  const page = args.object
  page.bindingContext = bindingContext
}

export async function SignIn(args) {
  const button = args.object
  const page = button.page
  const email = bindingContext.get('user').email
  const password = bindingContext.get('user').password
  // page.frame.navigate('tab-navigation/tab-navigation-page')

  const response = await Http.request({
    url: 'https://plant-doctor-db.vercel.app/api/users/login',
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    content: JSON.stringify({
      email,
      password,
    }),
  })
  if (response.statusCode === 200 || response.statusCode === 201) {
    const content = JSON.parse(response.content.toString()) // Parse JSON response
    if (content.error) {
      console.log('error:', content.error)
      alert('Error: ' + content.error) // Display error message
    } else {
      ApplicationSettings.setString('token', content.token)
      ApplicationSettings.setString('_id', content._id)
      console.log(content)
      page.closeModal()
    }
  } else {
    console.log('error:', response.statusCode, response.content.toJSON().error)
    alert(
      `Error: ${response.content.toJSON().error}, code : ${response.statusCode}`
    )
  }
}
export function onOpenSignUp(args) {
  const button = args.object
  const page = button.page

  const opts = {
    context: null,
    closeCallback: () => console.log('dialog closed'),
  }
  const modalPagePath = 'auth/signUp/signUp'
  const context = {
    closeCallback: opts.closeCallback,
  }

  page.showModal(
    modalPagePath,
    context,
    async () => {
      console.log('Dialog closed')
      // page.frame.navigate('tab-navigation/tab-navigation-page')
    },
    false
  )
  const signIn = args.object.page.getViewById('signIn')
  signIn.visibility = signIn.visibility === 'visible' ? 'collapsed' : 'visible'
}
