import { fromObject, Observable } from '@nativescript/core'
import { Http, ApplicationSettings } from '@nativescript/core'

export class SearchItemsViewModel extends Observable {
  constructor() {
    super()
    this.items = []
    this.loadItems()
  }

  async loadItems() {
    console.log('Loading')
    try {
      const userId = ApplicationSettings.getString('_id', 'defaultValue')
      console.log(userId)
      const response = await Http.request({
        url: 'https://plant-doctor-db.vercel.app/api/plants/getPlants',
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        content: JSON.stringify({
          userId,
        }),
      })
      if (response.statusCode === 200 || response.statusCode === 201) {
        const content = JSON.parse(response.content.toString()) // Parse JSON response
        if (content.error) {
          console.log('error:', content.error)
          alert('Error: ' + content.error) // Display error message
        } else {
          console.log(content)
          const extractedItems = content.map((item) => ({
            name: item.name,
            symptoms: item.symptoms,
            setting: item.setting,
            location: item.location,
          }))
          this.set('items', extractedItems)
        }
      } else {
        console.log(
          'error:',
          response.statusCode,
          response.content.toJSON().error
        )
      }
    } catch (error) {
      console.error('Error making HTTP request:', error)
    }
  }
}
