import { SearchItemsViewModel } from './search-view-model'

export function onNavigatingTo(args) {
  const component = args.object
  component.bindingContext = new SearchItemsViewModel()
}

export function onItemTap(args) {
  const view = args.view
  const page = view.page
  const tappedItem = view.bindingContext

  page.frame.navigate({
    moduleName: 'search/search-item-detail/search-item-detail-page',
    context: tappedItem,
    animated: true,
    transition: {
      name: 'slide',
      duration: 200,
      curve: 'ease',
    },
  })
}
