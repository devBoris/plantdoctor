import { fromObject } from '@nativescript/core'

export function CameraViewModel() {
  const viewModel = fromObject({})

  return viewModel
}
