import { CameraViewModel } from './camera-view-model'

export function onNavigatingTo(args) {
  const component = args.object
  component.bindingContext = new CameraViewModel()
}
