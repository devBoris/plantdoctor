import { InfoViewModel } from './browse-view-model'
import * as VideoPlayer from 'nativescript-videoplayer'

import { Video } from 'nativescript-videoplayer'

export function onNavigatingTo(args) {
  const component = args.object
  component.bindingContext = new InfoViewModel()
}

const video = topmost().currentPage.getViewById('nativeVideoPlayer')
// Setting event listeners on the Video
video.on(Video.pausedEvent, () => {
  console.log('Video has been paused.')
})

video.on(Video.mutedEvent, () => {
  console.log('Video has been muted.')
})

// changing the src
video.src = 'some video file or url'

// set loop
video.loop = false
