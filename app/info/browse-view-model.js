import { fromObject } from '@nativescript/core'

export function InfoViewModel() {
  const viewModel = fromObject({
    /* Add your view model properties here */
  })

  return viewModel
}
